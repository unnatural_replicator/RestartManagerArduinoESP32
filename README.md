# Restart Manager for Arduino

Going to restart? Do it with style! 

This library is tought as a default mode of restarting the ESP (or other arduino-based boards)

Can be used with esp32's `Preferences.h` (or not: see the section "`#define` flags")

The library itself is moddable for your use-case (with the "`#define` flags").


## Features

- restart with a user message and a standardised restart code

- and save those to be read on the next reboot

- an `on_restart` callback to clean up, dependent on the restart code you use 

## Usage

See `example/example.ino`

The main class is the `RestartManagerH::Restarter` class.

Let's say that you have a `RestartManagerH::Restarter restarter`,
to restart you simply call `restarter()` with or without arguments.
The optional arguments are `code`, the exact reason forthe restart defined in the enum Code, 
and `optional_message` that is a free-form user text.

### Callback functions on restart

To register a generic callback do `restarter.on_restart(callback)`, the callback is of type ` function<void(Code code, const String &message, void *userdata)>` so you can use lambda closures too.

To register a specific callback to `restarter.on_restart(code, callback)`.

The generic callback will be executed AFTER the specific callback.

### Getting data from previous restart
 
- `get_last_restart_reason_code()`  
- `get_last_restart_reason()`  
- `get_last_restart_timestamp()`  
- `get_last_restart_user_message()`  

NB: only if you didn't disabled `RESTARTMANAGER__PERSISTENCY`

### `#define` flags

Those modify the building of the library and must be defined before includin the library or they will have no effect.

- `RESTARTMANAGER__RESTART_FUNCTION`: custom restart function to port this code to other platforms that are not esp32; default: `ESP.restart()`

- `RESTARTMANAGER__PERSISTENCY`: write to nvm some restart data for the next time, disable to not include "Preferences.h" altogheter; default: `true`

- `RESTARTMANAGER__CODE`: an `enum:uint8_t` of message codes; default: see source

- `RESTARTMANAGER__MAP`: an `std::map<Code, String>` of textual representation of the message codes; default: see source

NB: `RESTARTMANAGER__CODE` and `RESTARTMANAGER__MAP` must be defined togheter and be the same length except that `RESTARTMANAGER__CODE` must have a maximum value of `__MAX_CODE`, that can be unmapped to `RESTARTMANAGER__MAP`
