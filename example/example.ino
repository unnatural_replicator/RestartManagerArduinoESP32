#define RESTARTMANAGER__PERSISTENCY false 
#include "RestartManager.h"


RestartManagerH::Restarter restarter;


void setup()
{
    Serial.begin(115200);

    // you can get a few info from the last session
    Serial.println("Last restart info:");
    Serial.print( "    reason code: ");    Serial.println( restarter.get_last_restart_reason_code()    );
    Serial.print( "    timestamp: "  );    Serial.println( restarter.get_last_restart_timestamp()      );
    Serial.print( "    reason text: ");    Serial.println( restarter.get_last_restart_reason()         );
    Serial.print( "    message: "    );    Serial.println( restarter.get_last_restart_user_message()   );

    // you can run custom code just before restarting
    // you can only define one specific callback for every Code

    // this is a generic callback 
    restarter.on_restart(RestartManagerH::UNKNOWN, [](RestartManagerH::Code code, String message, void* userdata){
        // userdata depends on your implementation, you can pass a custom struct etc... 
        
        // what can i do with this function? a lot of things...
            // maybe send logging data thorugh wifi 
            // or save to SD
            // close gracefully the TCP connections
            // send a goodbye message to MQTT
            // or put the arms of your robot in a resting/safe position

        Serial.println("Going to restart!");
        Serial.println(code);
        Serial.println(message);


        Serial.println("ADIÓS!");
    });
}

void loop()
{
    //  OH NO
    //  your code run into a (fake) critical error! 
    restarter(RestartManagerH::Code::ERROR_LOADING_CONFIGS, "here are some details: ...");
    
}