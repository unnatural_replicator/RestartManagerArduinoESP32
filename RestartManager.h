#pragma once

// provide a unified interface for restarting, with an optional reason and message to be saved to filesystem (if enabled)

// TODO:
//       provide a persistency adapter and take that as a pointer in constructor
//           common interface is a simplified ConfigStore.h interface, with file methods
//       callback takes a reference to the persistency adapter too (or nullptr)
//       set callback in constructor OR set_callback method ?

#include <Arduino.h>
#include <map>
#include <functional>
#include <time.h>

#ifndef RESTARTMANAGER__RESTART_FUNCTION
#define RESTARTMANAGER__RESTART_FUNCTION ESP.restart()
#endif // not defined(RESTARTMANAGER__RESTART_FUNCTION)

#ifndef RESTARTMANAGER__PERSISTENCY
#define RESTARTMANAGER__PERSISTENCY true
#endif // not defined(RESTARTMANAGER__PERSISTENCY)

// both need to be defined in order to function properly, if not it will ignore the user-defined value
#if not(defined(RESTARTMANAGER__CODE) and defined(RESTARTMANAGER__MAP))
#undef RESTARTMANAGER__CODE // must be based off uint8_t and end with an unmapped `__MAX_CODE`
#undef RESTARTMANAGER__MAP
#endif //not(defined(RESTARTMANAGER__CODE) and defined(RESTARTMANAGER__MAP))

#if RESTARTMANAGER__PERSISTENCY
#include "Preferences.h"
#endif // RESTARTMANAGER__PERSISTENCY

// disables esp32-specific logging 
#ifndef log_w
#define log_w(...)
#endif
#ifndef log_e
#define log_e(...)
#endif
#ifndef log_n
#define log_n(...)
#endif
#ifndef log_d
#define log_d(...)
#endif
#ifndef log_v
#define log_v(...)
#endif
#ifndef log_i
#define log_i(...)
#endif

namespace RestartManagerH
{

#ifndef RESTARTMANAGER__CODE
    enum Code : uint8_t
    {
        UNKNOWN,
        DEFAULT_CODE,
        NO_WIFI,
        NO_SERVER,
        TOO_MANY_DISCONNECTIONS,
        WIFI_CONNECTION_TIMEOUT,
        WIFI_DISCONNECTION_TIMEOUT,
        ERROR_LOADING_CONFIGS,
        __MAX_CODE // not mapped
    };
#else
    RESTARTMANAGER__CODE;
#endif // not defined(RESTARTMANAGER__CODE)
    using std::map;
    using std::make_pair;
    using std::function;
    using callback_t = function<void(Code code, const String &message, void *userdata)>;

    struct Callback{
        Code code;
        callback_t callback;
        void* userdata;
        // Callback(){}
        // Callback(Callback&& cb)
        // :code(cb.code), callback(cb.callback), userdata(cb.userdata)
        // {}
        Callback(Code code, callback_t callback, void* userdata=nullptr)
        :code(code), callback(callback), userdata(userdata)
        {}
        Callback(callback_t callback, void* userdata=nullptr)
        :code(Code::__MAX_CODE), callback(callback), userdata(userdata)
        {}

    };

    class Restarter
    {
        // used in the dictionary: the Code would be duplicated
        struct CallbackNoCode{
            callback_t callback;
            void* userdata;
            // CallbackNoCode(Callback callback)
            // :callback(callback.callback), userdata(callback.userdata)
            // {}
            CallbackNoCode(Callback&& callback)
            :callback(callback.callback), userdata(callback.userdata)
            {}
            CallbackNoCode(callback_t callback, void* userdata=nullptr)
            :callback(callback), userdata(userdata)
            {}

        };
        #if RESTARTMANAGER__PERSISTENCY
        Preferences persistency;
        #endif
        const char *persistency_folder;
        map<Code, CallbackNoCode> callbacks;
        bool init_done;
        Code restart_reason{Code::__MAX_CODE};
        String restart_message{""};
        String restart_timestamp{""}; // TODO
        static const map<Code, String> code2text_map;

        Code get_code_nothrow(Code code)
        {
            if (code2text_map.find(code) != code2text_map.end())
                return code;
            return Code::UNKNOWN;
        }
        String get_present_timestamp()
        {
            timeval tv;
            char timestamp_buffer[24];

            if (not gettimeofday(&tv, nullptr))
                snprintf(timestamp_buffer, 24, "%u", tv.tv_usec);
            else
                snprintf(timestamp_buffer, 24, "0");
            return String(timestamp_buffer);
        }
        // load old restart records. To be used if the RestartManager object has been declared in the global scope and you want to use persistency
        bool init()
        {
            #if RESTARTMANAGER__PERSISTENCY
            if (persistency_folder[0] == '\0')
            {
                log_i("No persistency folder set, not using persistency");
                return false;
            }

            persistency.begin(persistency_folder, true);
            restart_reason = (Code)persistency.getUChar("RM_code", (uint8_t)Code::UNKNOWN); // getString(restart_reason_pref_name, "unknown");
            restart_message = persistency.getString("RM_message");
            restart_timestamp = persistency.getString("RM_timestamp");
            persistency.end();

            bool all_loaded = true;
            if (all_loaded &= (restart_reason >= (uint8_t)Code::UNKNOWN))
                log_w("No saved restart code found or code unknown at restart time");
            if (all_loaded &= (restart_message.length() == 0))
                log_w("No saved restart message found");
            if (all_loaded &= (restart_timestamp.length() == 0))
                log_w("No saved restart timestamp found");
            init_done = true;
            return all_loaded;
            #else
            return false;
            #endif
        }

        bool insert_or_assign_callback(Code code, callback_t callback, void* callback_user_data){
            if (callbacks.find(code) == callbacks.end())
                callbacks.insert(make_pair(code, CallbackNoCode{callback, callback_user_data}));
            else
                callbacks.emplace(make_pair(code, CallbackNoCode{callback, callback_user_data}));
        }

    public:
        // if you want to use persistency, use `do_init==true` only in `setup` or after, NOT before
        Restarter(
            bool do_init = false,
            String persistency_folder = "restartManagerH",
            callback_t callback = nullptr,
            void *callback_user_data = nullptr)
            : persistency_folder(persistency_folder.c_str())
        {
            if (callback) insert_or_assign_callback(Code::__MAX_CODE, callback, callback_user_data);
            if (do_init)
                init();
        }
        Restarter(
            String persistency_folder,
            callback_t callback = nullptr,
            void *callback_user_data = nullptr)
            : persistency_folder(persistency_folder.c_str())
        {
            if (callback) insert_or_assign_callback(Code::__MAX_CODE, callback, callback_user_data);
        }
        Restarter(
            callback_t callback,
            void *callback_user_data = nullptr,
            String persistency_folder = "restartManagerH")
            : persistency_folder(persistency_folder.c_str())
        {
            if (callback) insert_or_assign_callback(Code::__MAX_CODE, callback, callback_user_data);
        }

        // set/unset callback. unset by not passing arguments or only the Code
        void on_restart(Code code, callback_t callback = nullptr, void *userdata = nullptr)
        {   
            insert_or_assign_callback(code, callback, userdata);
        }
        void on_restart(Callback callback){
            on_restart(callback.code, callback.callback, callback.userdata);
        }

        Code get_last_restart_reason_code(bool reload = false)
        {
            if (reload or not init_done)
                init();
            return restart_reason;
        }
        String get_last_restart_reason(bool reload = false)
        {
            if (reload or not init_done)
                init();
            return code2text_map.at(get_code_nothrow(restart_reason));
        }
        String get_last_restart_user_message(bool reload = false)
        {
            if (reload or not init_done)
                init();
            return restart_message;
        }
        String get_last_restart_timestamp(bool reload = false)
        {
            if (reload or not init_done)
                init();
            return restart_timestamp;
        }
        void restart(Code code = Code::UNKNOWN, const String optional_text = "") //, void *callback_user_data_ = nullptr)
        {
            log_w("Going to restart.\nReason: %s (%u)\n", restart_reason, restart_message);
            log_w("%s\n", optional_text.c_str());

            // timestamp
            restart_timestamp = get_present_timestamp();

            #if RESTARTMANAGER__PERSISTENCY
            bool all_saved = true;
            if (persistency_folder[0] != '\0')
            {
                log_i("Saving restart data to nvm, in persistency_folder %s");
                persistency.begin(persistency_folder, true);
                all_saved &= persistency.putUChar("RM_code", (uint8_t)code);
                all_saved &= persistency.putString("RM_message", optional_text);
                all_saved &= persistency.putString("RM_timestamp", restart_timestamp);
                persistency.end();
                if (not all_saved)
                    log_w("Cannot save some restart values for next time, some values can be stale.");
                delay(2);
            }
            #endif

            // call callback
            auto cb_obj = callbacks.at(code);
            if (cb_obj.callback) {
                log_d("Executing `on_restart` callback for code %u: '%s'", code, code2text_map.at(code) );            
                cb_obj.callback(code, optional_text, cb_obj.userdata);}
            auto gen_cb_obj = callbacks.at(__MAX_CODE); //generic
            if (gen_cb_obj.callback) {
                log_d("Executing generic `on_restart` callback", code, code2text_map.at(code) );            
                gen_cb_obj.callback(code, optional_text, gen_cb_obj.userdata);}
            delay(2);
            RESTARTMANAGER__RESTART_FUNCTION;
        }
        void operator()(Code code = Code::UNKNOWN, String optional_text = "") //, void* callback_user_data=nullptr)
        {
            restart(code, optional_text); //, callback_user_data);
        }
    };

    const map<Code, String> Restarter::code2text_map{
#ifndef RESTARTMANAGER__MAP
        {Code::UNKNOWN, "Unknown or not found."},
        {Code::DEFAULT_CODE, "Restart called with `DEFAULT_CODE`: not specified."},
        {Code::NO_WIFI, "No wifi network found."},
        {Code::NO_SERVER, "Server not found."},
        {Code::TOO_MANY_DISCONNECTIONS, "Too many disconnections."},
        {Code::WIFI_CONNECTION_TIMEOUT, "Connection timeout."},
        {Code::WIFI_DISCONNECTION_TIMEOUT, "Has been disconnected."},
        {Code::ERROR_LOADING_CONFIGS, "Cannot load configs."}
#else
        RESTARTMANAGER__MAP
#endif
    };
};
